import './app';
import $ from 'jquery';
import angular from 'angular';

$(document).ready(function() {
  const appContainer = $('#agenda')[0];

  angular.bootstrap(appContainer, ['app'], {
    strictDi: true
  });
});
