import './auth.styl';

import run from './auth.run';
import angular from 'angular';
import config from './auth.config';
import SignInComponent from './signin';
import SignUpComponent from './signup';
import AuthService from './auth.service';

export default angular.module('app.auth', [ 'ui.router', 'ngResource' ])
  .run(run)
  .config(config)
  .component('signIn', SignInComponent)
  .component('signUp', SignUpComponent)
  .service("AuthService", AuthService);
