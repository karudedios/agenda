export default class AuthService {
  constructor($resource) {
    'ngInject';

    Object.assign(this, {
      api: $resource('/auth/:action', { }),
      userApi: $resource('/api/users/me', {}, {})
    });
  }

  signIn({ username, password }) {
    return this.api.save({ action: 'signin' }, {
      username,
      password
    }).$promise;
  }

  signOut() {
    return this.api.save({ action: 'signout' }, {}).$promise;
  }

  signUp({ username, password, firstName, lastName, phoneNumber }) {
    return this.api.save({ action: 'signup' }, {
      username,
      password,
      firstName,
      lastName,
      phoneNumber
    }).$promise;
  }

  isAuthenticated() {
    return this.api.get({ action: 'authenticated' }).$promise;
  }

  activeUser() {
    return this.userApi.get({}).$promise;
  }
}
