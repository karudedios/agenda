class SignupController {
  constructor(AuthService, $state) {
    'ngInject';
    Object.assign(this, { AuthService, $state });
  }

  authenticate() {
    delete this.error;

    this.AuthService.signUp(this).then(() => {
      this.$state.go('agenda.index');
    }).catch(err => {
      console.warn(err);
      this.error = err.data;
    });
  }

  invalidPassword(){
    return this.password !== this.password2;
  }
}

export default {
  controllerAs: 'signup',
  controller: SignupController,
  template: require('./signup.tpl.html')
};
