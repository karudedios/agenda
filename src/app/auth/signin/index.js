class SigninController {
  constructor(AuthService, $state) {
    'ngInject';
    Object.assign(this, { AuthService, $state });
  }

  authenticate() {
    delete this.error;

    this.AuthService.signIn(this).then(() => {
      this.$state.go('agenda.index');
    }).catch(err => {
      console.warn(err);
      this.error = err.data;
    });
  }
}

export default {
  controllerAs: 'signin',
  controller: SigninController,
  template: require('./signin.tpl.html')
};
