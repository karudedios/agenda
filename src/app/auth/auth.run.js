export default function AuthRun($transitions) {
  'ngInject';

  $transitions.onBefore({
    entering: 'auth.**'
  }, (trans) => {
    if (trans.to().name === 'auth.signout') return true;

    const AuthService = trans.injector().get('AuthService');

    return AuthService
      .isAuthenticated()
      .then(({ isAuthenticated }) => {
        return isAuthenticated ? trans.router.stateService.target('agenda.index') : true;
      });
  });
}
