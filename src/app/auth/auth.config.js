export default function AuthConfig($stateProvider) {
  'ngInject';

  $stateProvider
    .state('auth', {
      url: '',
      abstract: true,
      template: require('./auth.tpl.html')
    })
    .state('auth.signin', {
      url: '/signin',
      component: 'signIn'
    })
    .state('auth.signup', {
      url: '/signup',
      component: 'signUp'
    })
    .state('auth.signout', {
      url: '/signout',
      controller: function(AuthService, $state) {
        'ngInject';

        AuthService.signOut()
          .then(() => $state.go('auth.signin'));
      }
    });
}
