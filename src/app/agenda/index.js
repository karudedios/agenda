import './events';
import './contacts';

import angular from 'angular';
import run from './agenda.run';
import config from './agenda.config';
import IndexComponent from './index/';
import AgendaService from './agenda.service';
import AgendaComponent from './agenda.component';


export default angular.module('app.agenda', [
  'ui.router',
  'ngResource',
  'app.agenda.events',
  'app.agenda.contacts'
])
  .run(run)
  .config(config)
  .component('index', IndexComponent)
  .component('agenda', AgendaComponent)
  .service('AgendaService', AgendaService);
