import $ from 'jquery';

class NavItem {
  constructor(text, sref) {
    Object.assign(this, { text, sref });
  }
}

class AgendaCtrl {
  constructor() {
    Object.assign(this, {
      nav: [
        new NavItem("Inicio", "agenda.index"),
        new NavItem("Eventos", "agenda.events.list"),
        new NavItem("Contactos", "agenda.contacts.list"),
        new NavItem("Cerrar Sesión", "auth.signout"),
      ]
    });
  }

  $postLink() {
    $(document).ready(function() {
      $(".button-collapse").sideNav({
        closeOnClick: true
      });
    });
  }
}


export default {
  bindings: {
    user: '<'
  },

  controllerAs: 'agenda',
  controller: AgendaCtrl,
  template: require('./agenda.tpl.html')
};
