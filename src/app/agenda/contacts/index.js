import './contacts.styl';

import angular from 'angular';
import config from './contacts.config';
import contactListComponent from './contact-list';
import contactsComponent from './contacts.component';
import contactDetailComponent from './contact-detail';

export default angular.module('app.agenda.contacts', [])
  .config(config)
  .component('contacts', contactsComponent)
  .component('contactList', contactListComponent)
  .component('contactDetail', contactDetailComponent);
