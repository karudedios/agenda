export default {
  bindings: {
    'user': '<'
  },

  controllerAs: 'contacts',
  template: require('./contacts.tpl.html')
};
