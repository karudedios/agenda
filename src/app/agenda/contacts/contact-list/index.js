import './contact-list.styl';

class ContactListCtrl {
  $onInit() {
    if (this.parent) {
      this.parent.canCreate = true;
      this.parent.header = "Contactos";
    }
  }
}

export default {
  bindings: {
    'contacts': '<'
  },

  require: {
    'parent': '^^?contacts'
  },

  controller: ContactListCtrl,
  controllerAs: 'contactList',
  template: require('./contact-list.tpl.html')
};
