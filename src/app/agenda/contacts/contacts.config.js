export default function ContactsConfig($stateProvider) {
  'ngInject';

  $stateProvider
    .state('agenda.contacts', {
      abstract: true,
      url: '/contactos',
      component: 'contacts'
    })
    .state('agenda.contacts.list', {
      url: '',
      component: 'contactList',
      resolve: {
        'contacts': (AgendaService) => {
          'ngInject';
          const options = { sort: { name: 1 } };
          return AgendaService.findContacts({ options });
        }
      }
    })
    .state('agenda.contacts.new', {
      url: '/nuevo',
      component: 'contactDetail',
      resolve: {
        createMode: () => true,

        contact: (AgendaService) => {
          'ngInject';
          return AgendaService.emptyContact();
        }
      }
    })
    .state('agenda.contacts.detail', {
      url: '/:id',
      component: 'contactDetail',
      resolve: {
        'contact': ($stateParams, AgendaService) => {
          'ngInject';
          return AgendaService.findContactById($stateParams.id);
        }
      }
    });
}
