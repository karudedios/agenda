class ContactDetailCtrl {
  constructor($state) {
    'ngInject';
    Object.assign(this, { $state });
  }

  $onInit() {
    if (!this.contact) {
      this.contact = {};
    }

    if (this.parent) {
      this.parent.canCreate = false;
      this.parent.header = this.contact.fullname || "Nuevo Contacto";
    }
  }

  create() {
    Object.assign(this, { loading: true });

    return this.contact.$save()
      .then(({ id }) =>
        this.$state.go('agenda.contacts.detail', { id }))
      .catch(err =>
        this.error = err)
      .finally(() => {
        this.loading = false;
      });
  }

  update() {
    Object.assign(this, { loading: true });

    return this.contact.$update()
      .then(contact => {
        if(this.parent) {
          this.parent.header = contact.fullname;
        }
      })
      .catch(err =>
        this.error = err)
      .finally(() => {
        this.loading = false;
      });
  }

  delete() {
    return this.contact.$remove()
      .then(() => this.$state.go('agenda.contacts.list'))
      .catch(err => this.error = err);
  }

  onSubmit() {
    if (this.createMode) {
      this.create();
    } else {
      this.update();
    }
  }
}

export default {
  bindings: {
    'contact': '<',
    'createMode': '<?'
  },

  require: {
    'parent': '^^?contacts'
  },

  controller: ContactDetailCtrl,
  controllerAs: 'contactDetail',
  template: require('./contact-detail.tpl.html')
};
