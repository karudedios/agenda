export default class AgendaService {
  constructor($resource) {
    'ngInject';

    Object.assign(this, {
      Agenda: $resource('/api/agenda/:kind/:id', {
        "id": "@id",
        "kind": "@kind",
      }, {
        'update': { method: 'PUT' }
      })
    });
  }

  find(kind, predicate) {
    return this.Agenda.query(Object.assign({ kind }, predicate)).$promise;
  }

  findContacts(predicate = {}) {
    return this.find('contacts', predicate);
  }

  findEvents(predicate = {}) {
    return this.find('events', predicate);
  }

  findOne(kind, predicate = {}) {
    return this.Agenda.get(Object.assign({ kind }, predicate)).$promise;
  }

  findContactById(id) {
    return this.findOne('contacts', { id });
  }

  findEventById(id) {
    return this.findOne('events', { id });
  }

  emptyContact() {
    return new this.Agenda({ kind: "contacts" });
  }

  emptyEvent() {
    return new this.Agenda({ kind: "events" });
  }
}
