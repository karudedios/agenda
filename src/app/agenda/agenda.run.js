export default function agendaRun($transitions, $http, $httpParamSerializerJQLike) {
  'ngInject';

  $transitions.onBefore({
    entering: 'agenda.**'
  }, (trans) => {
    const AuthService = trans.injector().get('AuthService');

    return AuthService.isAuthenticated()
      .then(({ isAuthenticated }) => {
        return isAuthenticated ? true : trans.router.stateService.target('auth.signin');
      });
  });

  $http.defaults.paramSerializer = $httpParamSerializerJQLike;
}
