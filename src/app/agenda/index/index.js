class AgendaIndex {

}

export default {
  bindings: {
    'recentEvents': '<',
    'recentContacts': '<'
  },

  controllerAs: 'index',
  controller: AgendaIndex,
  template: require('./index.tpl.html')
};
