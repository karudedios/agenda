export default function agendaConfig($stateProvider) {
  'ngInject';

  $stateProvider
    .state('agenda', {
      url: '',
      abstract: true,
      component: 'agenda',

      resolve: {
        'user': (AuthService) => AuthService.activeUser()
      }
    })
    .state('agenda.index', {
      url: '/',
      component: 'index',

      resolve: {
        recentEvents: function(AgendaService) {
          'ngInject';
          const options = { sort: { 'date': -1 }, limit: 10 };
          return AgendaService.findEvents({ options });
        },

        recentContacts: function(AgendaService) {
          'ngInject';
          const options = { sort: { 'created_at': -1 }, limit: 10 };
          return AgendaService.findContacts({ options });
        }
      }
    });
}
