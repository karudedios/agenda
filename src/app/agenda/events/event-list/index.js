import './event-list.styl';

class EventListCtrl {
  $onInit() {
    if (this.parent) {
      this.parent.canCreate = true;
      this.parent.header = "Eventos";
    }
  }
}

export default {
  bindings: {
    'events': '<'
  },

  require: {
    'parent': '^^?events'
  },

  controller: EventListCtrl,
  controllerAs: 'eventList',
  template: require('./event-list.tpl.html')
};
