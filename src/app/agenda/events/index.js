import './events.styl';

import angular from 'angular';
import config from './events.config';
import eventListComponent from './event-list';
import eventsComponent from './events.component';
import eventDetailComponent from './event-detail';

export default angular.module('app.agenda.events', [])
  .config(config)
  .component('events', eventsComponent)
  .component('eventList', eventListComponent)
  .component('eventDetail', eventDetailComponent);
