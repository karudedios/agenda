export default function EventsConfig($stateProvider) {
  'ngInject';

  $stateProvider
    .state('agenda.events', {
      abstract: true,
      url: '/eventos',
      component: 'events'
    })
    .state('agenda.events.list', {
      url: '',
      component: 'eventList',
      resolve: {
        'events': (AgendaService) => {
          'ngInject';
          const options = { sort: { name: 1 } };
          return AgendaService.findEvents({ options });
        }
      }
    })
    .state('agenda.events.new', {
      url: '/nuevo',
      component: 'eventDetail',
      resolve: {
        createMode: () => true,

        event: (AgendaService) => {
          'ngInject';
          return AgendaService.emptyEvent();
        }
      }
    })
    .state('agenda.events.detail', {
      url: '/:id',
      component: 'eventDetail',
      resolve: {
        'event': ($stateParams, AgendaService) => {
          'ngInject';
          return AgendaService.findEventById($stateParams.id);
        }
      }
    });
}
