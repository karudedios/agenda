import $ from 'jquery';

class EventDetailCtrl {
  constructor($state, $timeout, $scope) {
    'ngInject';
    Object.assign(this, { $state, $timeout, $scope });
  }

  $onInit() {
    if (!this.event) {
      this.event = {};
    } else {
      this.event.date = new Date(this.event.date);
    }

    if (this.parent) {
      this.parent.canCreate = false;
      this.parent.header = this.event.name || "Nuevo Evento";
    }
  }

  $postLink() {
    $(document).ready(() => {
      $('input[type=date]').pickadate({
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec'],
        weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        showMonthsShort: true,

        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Cancelar',

        labelMonthNext: 'Próximo Mes',
        labelMonthPrev: 'Mes Anterior',

        firstDay: 1,
        format: 'dd mmm, yyyy',

        onSet: ({ select }) => {
          this.event.date = new Date(select);
          this.$scope.$apply();
        }
      });
    });
  }

  create() {
    Object.assign(this, { loading: true });

    return this.event.$save()
      .then(({ id }) =>
        this.$state.go('agenda.events.detail', { id }))
      .catch(err =>
        this.error = err)
      .finally(() => {
        this.loading = false;
      });
  }

  update() {
    Object.assign(this, { loading: true });

    return this.event.$update()
      .then(event => {
        event.date = new Date(event.date);

        if(this.parent) {
          this.parent.header = event.name;
        }
      })
      .catch(err =>
        this.error = err)
      .finally(() => {
        this.loading = false;
      });
  }

  delete() {
    return this.event.$remove()
      .then(() => this.$state.go('agenda.events.list'))
      .catch(err => this.error = err);
  }

  onSubmit() {
    if (this.createMode) {
      this.create();
    } else {
      this.update();
    }
  }
}

export default {
  bindings: {
    'event': '<',
    'createMode': '<?'
  },

  require: {
    'parent': '^^?events'
  },

  controller: EventDetailCtrl,
  controllerAs: 'eventDetail',
  template: require('./event-detail.tpl.html')
};
