export default {
  bindings: {
    'user': '<'
  },

  controllerAs: 'events',
  template: require('./events.tpl.html')
};
