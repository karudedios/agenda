import './auth';
import './agenda';
import './app.styl';

import angular from 'angular';
import config from './app.config';

export default angular.module('app', [ 'ui.router', 'app.auth', 'app.agenda' ])
  .config(config);
