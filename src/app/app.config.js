import './app.styl';

export default function appConfig($stateProvider, $urlRouterProvider) {
  'ngInject';

  $urlRouterProvider.otherwise($injector =>
    $injector.get('$state').go('agenda.index'));
}
