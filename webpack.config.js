const { resolve } = require('path');
const webpack     = require('webpack');
const HappyPack   = require('happypack');
const HtmlWebpack = require('html-webpack-plugin');
const ExtractText = require('extract-text-webpack-plugin');

module.exports = {
  devtool: 'source-maps',

  entry: {
    app: './src/index.js',
    vendor: './src/vendor.js',
    materialize: 'materialize-loader!./materialize.config.js'
  },

  resolve: {
    extensions: ['.js'],
    unsafeCache: /node_modules/,
    modules: [resolve('src'), 'node_modules'],

    alias: {
      'jquery': resolve('node_modules', 'jquery', 'dist', 'jquery'),
    }
  },

  output: {
    filename: 'js/[name].bundle.js',
    path: resolve('public')
  },

  module: {
    rules: [{
      test: require.resolve('angular'),
      loader: 'exports-loader?window.angular'
    }, {
      test: /\.js$/,

      include: /src/,
      exclude: /node_modules/,

      use: ['happypack/loader?id=js']
    }, {
      test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      use: "url-loader?limit=10000&mimetype=application/font-woff"
    }, {
      test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      use: "file-loader"
    }, {
      test: /\.styl$/,

      include: /src/,
      exclude: /node_modules/,

      use: ExtractText.extract({
        use: ['happypack/loader?id=stylus']
      }),
    }, {
      test: /\.html$/,

      include: /src/,
      exclude: /node_modules/,

      use: 'html-loader'
    }]
  },

  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.$': 'jquery',
      'window.jQuery': 'jquery'
    }),

    new HappyPack({
      id: 'js',
      threads: 4,
      loaders: [
        'ng-annotate-loader?es6',
        'babel-loader?retainLines&cacheDirectory',
        'eslint-loader?cache&emitError&emitWarning'
      ]
    }),

    new HappyPack({
      id: 'stylus',
      threads: 2,
      loaders: [
        'css-loader',
        'stylus-loader'
      ]
    }),

    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest'],
      minChuncks(module, count) {
        return count < 2;
      }
    }),

    new ExtractText('./css/app.css'),

    new HtmlWebpack({
      path: resolve('public'),
      template: resolve('src/index.html')
    })
  ]
};
