const path            = require('path');
const http            = require('http');
const express         = require('express');
const passport        = require('passport');
const mongoose        = require('mongoose');
const bodyParser      = require('body-parser');
const router          = require('./server/routing');
const cookieParser    = require('cookie-parser');
const cookieSession   = require('cookie-session');

const app     = express();
const server  = http.createServer(app);

app
  .use(cookieParser())
  .use(cookieSession({
    key: "cookieKey",
    secret: "secret cookie",
    cookie: { maxAge: 1000 * 60 * 24 }
  }))
  .use(passport.initialize())
  .use(passport.session())
  .use(router(
    passport,
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true })
  ))
  .use(express.static(path.resolve('public')));

const [ ip, port ] = [
  process.env.IP || '0.0.0.0',
  process.env.PORT || '8080'
];

server.listen(port, ip, () => {
  mongoose.Promise = require('bluebird');
  mongoose.connect('mongodb://localhost');
  console.log(`Server running ${ip}:${port}`);
});
