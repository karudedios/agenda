const authRouter    = require('./auth');
const userRouter    = require('./user');
const { Router }    = require('express');
const agendaRouter  = require('./agenda');

const configureAuthStrategies = require('./auth/strategies');

module.exports = function(passport, ...middlewares) {
  configureAuthStrategies(passport);

  return new Router()
    .use(middlewares)
    .use('/auth', authRouter(passport, requireAuthenticated, requireUnauthenticated))
    .use('/api', new Router()
      .use('/users' , userRouter(requireAuthenticated))
      .use('/agenda', agendaRouter(requireAuthenticated)));

  function requireAuthenticated(req, res, next) {
    if (!req.isAuthenticated()) {
      return res.status(401).send({ message: "No está autorizado para acceder a esta ruta." });
    }

    next();
  }

  function requireUnauthenticated(req, res, next) {
    if (req.isAuthenticated()) {
      return res.status(403).send({ message: "No puede llevar a cabo esta acción con un usuario ya autorizado." });
    }

    next();
  }
};
