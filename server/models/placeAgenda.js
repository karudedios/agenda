const Agenda    = require('./agenda');
const mongoose  = require('mongoose');

const PlaceAgendaSchema = Agenda.discriminator('PlaceAgenda', new mongoose.Schema({
  location: {
    type: String,
    required: true
  }
}));

module.exports = mongoose.model('PlaceAgenda', PlaceAgendaSchema);
