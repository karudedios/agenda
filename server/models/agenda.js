const _         = require('lodash');
const mongoose  = require('mongoose');

const AgendaSchema = new mongoose.Schema({
  created_at: Date,
  updated_at: Date,

  owner: {
    ref: 'User',
    type: mongoose.Schema.ObjectId
  }
}, {
  discriminatorKey: 'kind',
  toObject: { virtuals: true }
});

AgendaSchema.pre('save', function(next) {
  if (this.created_at) {
    this.updated_at = new Date();
  } else {
    this.created_at = new Date();
  }

  next();
});

AgendaSchema
  .virtual('fullname')
  .get(function() {
    return `${this.firstname} ${this.lastname || ''}`.trim();
  });

AgendaSchema.methods.toJSON = function() {
  return _.omit(this.toObject(), [
    '_id',
    '__v',
    'owner',
    'created_at',
    'updated_at'
  ]);
};

const Agenda = module.exports.Agenda = mongoose.model('Agenda', AgendaSchema);

module.exports.Contact = Agenda.discriminator('contacts', new mongoose.Schema({
  alias: String,

  address: String,

  firstname: {
    type: String,
    required: [true, "Name is Required"]
  },

  lastname: {
    type: String
  },

  phoneNumber: {
    type: String,
    validate: {
      validator: (value) => /\(?\d{3}\)?[ -]?\d{3}[ -]?\d{4}/.test(value),
      message: "{VALUE} is not a valid phone number"
    },
    required: [true, "User phone number required"]
    }
}));

module.exports.Event = Agenda.discriminator('events', new mongoose.Schema({
  place: {
    type: String,
    required: [true, "Place is required"]
  },

  name: {
    type: String,
    required: [true, "Name is Required"]
  },

  desc: {
    type: String,
    required: [true, "Desc is Required"]
  },

  date: {
    type: Date,
    required: [true, "Date is required"]
  }
}));
