const _         = require('lodash');
const mongoose  = require('mongoose');
const Promise   = require('bluebird');
const bcrypt    = Promise.promisifyAll(require('bcrypt'));

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    index: { unique: true },
    required: [true, "Username required"]
  },

  password: {
    type: String,
    required: [true, "Password required"]
  },

  info: {
    type: {
      firstname: String,
      lastname: String
    },

    default: {}
  }
}, {
  toObject: { virtuals: true }
});

UserSchema.pre('save', function(next) {
  if (!this.isModified('password')) return next();

  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(this.password, salt, (err, hash) => {
      this.password = hash;
      next();
    });
  });
});

UserSchema.methods.passwordMatches = function(pwd) {
  return bcrypt.compareAsync(pwd, this.password);
};

UserSchema.methods.toJSON = function() {
  return _.omit(this.toObject(), [ 'password', '__v', '_id' ]);
};

UserSchema
  .virtual('fullname')
  .get(function() {
    return this.info.firstname || this.info.lastname
      ? `${this.info.firstname || ''} ${this.info.lastname || ''}`.trim()
      : this.username;
  });


module.exports = mongoose.model('User', UserSchema);
