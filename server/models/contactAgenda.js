const Agenda    = require('./agenda');
const mongoose  = require('mongoose');

const ContactAgendaSchema = Agenda.discriminator('ContactAgenda', new mongoose.Schema({
  contact: {
    ref: 'User',
    required: true,
    type: mongoose.Schema.ObjectId
  }
}));

module.exports = mongoose.model('ContactAgenda', ContactAgendaSchema);
