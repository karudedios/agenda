const User          = require('../models/user');
const LocalStrategy = require('passport-local').Strategy;

module.exports = function configureAuthStrategies(passport) {
  passport.use('local', new LocalStrategy((username, password, done) => {
    User.findOne({ username })
      .then(user => {
        if (!user) {
          return done(null, false, { status: 404, message: "Usuario no existe" });
        }

        return user.passwordMatches(password)
          .then(matches => {
            if (matches) {
              return done(null, user);
            }

            done(null, false, { status: 400, message: "Contraseña incorrecta" });
          });
      }).catch(done);
  }));

  passport.serializeUser((user, done) => {
    done(null, user._id);
  });

  passport.deserializeUser((id, done) => {
    User.findById(id)
      .catch(done)
      .then(done.bind(null, null));
  });
};
