const { Router }  = require('express');
const User        = require('../models/user');

module.exports = function(passport, requireAuthenticated, requireUnauthenticated) {
  return new Router()
    .get('/authenticated', (req, res) => {
      res.json({ isAuthenticated: !!req.user });
    })
    .post('/signin', requireUnauthenticated, (req, res, next) => {
      passport.authenticate('local', (err, user, info) => {
        if(err) {
          return res.status(500).json(err);
        } else if (info) {
          return res.status(info.status).json(info);
        }

        req.login(user, (err) => {
          if (err) {
            return res.status(500).json(err);
          }

          return res.send("OK");
        });
      })(req, res, next);
    })
    .post('/signout', requireAuthenticated, (req, res) => {
      req.logout();
      res.send("OK");
    })
    .post('/signup', requireUnauthenticated, (req, res) => {
      User.findOne({ username: req.body.username })
        .then(user => {
          console.log(user);

          if (user){
            return res.status(400).json({ message: "Usuario ya existe" });
          }

          User.create(req.body)
            .then(user => {
              passport.authenticate('local')(req, res, function() {
                req.login(user, (err) => {
                  if (err) {
                    return res.status(500).json(err);
                  }

                  return res.send("OK");
                });
              });
            })
            .catch(err => {
              res.status(500).send(err);
            });
        });
    });
};
