const _                           = require('lodash');
const { Router }                  = require('express');
const { Agenda, Contact, Event }  = require('../models/agenda');

module.exports = function(...middlewares) {

  const cleanOptions = (options) =>
    _.assign({}, options, options.limit ? { limit: +options.limit } : {});

  return new Router()
    .use(middlewares)

    .get('/', (req, res) => {
      Agenda.find({ owner: req.user._id })
        .then(all => res.json(all))
        .catch(err => res.status(500).json({ message: err.message }));
    })

    .get('/contacts', (req, res) => {
      Contact.find({ owner: req.user._id }, {}, cleanOptions(req.query.options || {}))
        .then(contacts => res.json(contacts))
        .catch(err => res.status(500).json({ message: err.message }));
    })

    .get('/contacts/:id', (req, res) => {
      Contact.findOne({ _id: req.params.id, owner: req.user._id })
        .then(contact => res.json(contact))
        .catch(err => res.status(500).json({ message: err.message }));
    })

    .put('/contacts/:id', (req, res) => {
      Contact.findByIdAndUpdate(req.params.id, req.body, { new: true })
        .then(c => res.json(c))
        .catch(err => res.status(500).json({ message: err.message }));
    })

    .post('/contacts', (req, res) => {
      Contact.create(Object.assign({ owner: req.user._id }, req.body))
        .then(contact => res.json(contact))
        .catch(err => res.status(500).json({ message: err.message }));
    })

    .delete('/contacts/:id', (req, res) => {
      Contact.findByIdAndRemove({ _id: req.params.id })
        .then(() => res.send("OK"))
        .catch(err => res.status(500).json({ message: err.message }));
    })


    .get('/events', (req, res) => {
      Event.find({ owner: req.user._id }, {}, cleanOptions(req.query.options || {}))
        .then(events => res.json(events))
        .catch(err => res.status(500).json({ message: err.message }));
    })

    .get('/events/:id', (req, res) => {
      Event.findOne({ _id: req.params.id, owner: req.user._id })
        .then(event => res.json(event))
        .catch(err => res.status(500).json({ message: err.message }));
    })

    .put('/events/:id', (req, res) => {
      Event.findByIdAndUpdate(req.params.id, req.body, { new: true })
        .then(c => res.json(c))
        .catch(err => res.status(500).json({ message: err.message }));
    })

    .post('/events', (req, res) => {
      Event.create(Object.assign({ owner: req.user._id }, req.body))
        .then(place => res.json(place))
        .catch(err => res.status(500).json({ message: err.message }));
    });
};
