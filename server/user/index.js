const User        = require('../models/user');
const { Router }  = require('express');

module.exports = function (...middlewares) {
  return new Router()
    .use(middlewares)
    .get('/', (req, res) => {
      User.find({})
        .then(all => res.json(all))
        .catch(err => res.status(500).json({ message: err.message }));
    })

    .get('/me', (req, res) => res.json(req.user))

    .patch('/me', (req, res) => {
      req.user.update(req.body)
        .then(() => res.json(req.body))
        .catch(err => res.status(500).json({ message: err.message }));
    });
};
